//
//  HistoryViewController.swift
//  Wi_Fi_passwords_from_airports
//
//  Created by User543 on 29.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HistoryViewController: UIViewController, GADBannerViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var adsView: GADBannerView!
    
    var historyList = [NSMutableDictionary]()
    weak var handleMapSearchDelegate: HandleMapSearch?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.object(forKey: "historyList") != nil {
            self.historyList = UserDefaults.standard.object(forKey: "historyList") as! [NSMutableDictionary]
        } else {
            self.historyList = []
        }
        self.tableView.reloadData()
        self.checkProduct()
    }
    
    func checkProduct() {
        if !StoreManager.shared.isPurchased(id: nonconsumable1) {
            self.adsView.adUnitID = keyGoogleAdsBanner
            self.adsView.delegate = self
            self.adsView.rootViewController = self
            let request = GADRequest()
            self.adsView.load(request)
            self.adsView.isHidden = false
        } else {
            adsView.isHidden = true
        }
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Ads load successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Error \(error.localizedDescription)")
    }
}

extension HistoryViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell")!
        let selectedItem = self.historyList[indexPath.row]
        if selectedItem.object(forKey: "nameWifi") != nil {
            cell.textLabel?.text = selectedItem.object(forKey: "nameWifi") as? String
            cell.imageView?.image = #imageLiteral(resourceName: "user_pin_tab")
        } else {
            cell.textLabel?.text = selectedItem.object(forKey: "nameAirport") as? String
            cell.imageView?.image = #imageLiteral(resourceName: "airport")
        }
        return cell
    }
}

extension HistoryViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = self.historyList[indexPath.row]
        handleMapSearchDelegate?.dropPinZoomIn(placemark: selectedItem)
        navigationController!.tabBarController?.selectedIndex = 0
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
