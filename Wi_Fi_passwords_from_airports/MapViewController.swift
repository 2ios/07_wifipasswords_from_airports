//
//  ViewController.swift
//  Wi_Fi_passwords_from_airports
//
//  Created by User543 on 25.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMobileAds

protocol HandleMapSearch: class {
    func dropPinZoomIn(placemark: NSMutableDictionary)
}

class MapViewController: UIViewController, UIWebViewDelegate, CLLocationManagerDelegate, MKMapViewDelegate, GADBannerViewDelegate, UISearchControllerDelegate, UISearchBarDelegate, UITextFieldDelegate {
    
    var webView: UIWebView! = UIWebView(frame: CGRect.zero)
    var arrayOfAirports = NSArray()
    var listOfAirports = [NSMutableDictionary]()

    var listOfUserWifi = [NSMutableDictionary]()
    
    var callNumber = 0
    let locationManager = CLLocationManager()
    var selectedPin: NSMutableDictionary?
    var selectedAnnotation: MKPointAnnotation?
    var mapType = 0
    var resultSearchController: UISearchController?
    var searchController: UITableViewController?
    var alert: UIAlertController?
    var count = 0
    
    var pinImages = ["airportIcon", "user_pin_changed"]
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet var infoView: UIView!
    @IBOutlet var addWifi: UIView!
    
    @IBOutlet weak var nameAirport: UILabel!
    @IBOutlet weak var descriptionAirport: UILabel!
    
    @IBOutlet weak var inputWifiName: UITextField!
    @IBOutlet weak var inputDescriptionWifi: UITextField!
    

    @IBOutlet weak var infoViewNameLabel: UILabel!
    @IBOutlet weak var infoViewDescriptionLabel: UILabel!
    
    @IBOutlet weak var adsView: GADBannerView!
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.infoView.removeFromSuperview()
        self.mapButton.isHidden = true
    }
    
    @IBAction func closeButtonAddWifi(_ sender: UIButton) {
        self.addWifi.removeFromSuperview()
        self.mapButton.isHidden = true
        
        let dictUser = UserDefaults.standard.object(forKey: "user") as! NSDictionary
        let mutableDictUser = dictUser.mutableCopy() as! NSMutableDictionary
        mutableDictUser.setObject(inputWifiName.text!, forKey: "nameWifi" as NSCopying)
        mutableDictUser.setObject(inputDescriptionWifi.text!, forKey: "descriptionWifi" as NSCopying)
        self.listOfUserWifi.append(mutableDictUser)
        UserDefaults.standard.set(self.listOfUserWifi, forKey: "listOfUserWifi")
        
        let latitude = dictUser.object(forKey: "latitude") as! Double
        let longitude = dictUser.object(forKey: "longitude") as! Double
        let annotation = CustomPointAnnotation()
        annotation.title = inputWifiName.text!
        annotation.subtitle = inputDescriptionWifi.text!
        annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        annotation.number = 1
//        self.mapView.showsUserLocation = false
        self.mapView.addAnnotation(annotation)
        
        var historyList = [NSMutableDictionary]()
        if UserDefaults.standard.object(forKey: "historyList") != nil {
            historyList = UserDefaults.standard.object(forKey: "historyList") as! [NSMutableDictionary]
        } else {
            historyList = []
        }
        historyList.insert(mutableDictUser, at: 0)
        UserDefaults.standard.set(historyList, forKey: "historyList")
        UserDefaults.standard.synchronize()
        self.inputWifiName.text = ""
        self.inputDescriptionWifi.text = ""
    }
    
    @IBOutlet weak var mapButton: UIButton!
    
    @IBAction func mapViewButton(_ sender: UIButton) {
        self.infoView.removeFromSuperview()
        self.mapButton.isHidden = true
        self.addWifi.isHidden = true
    }
    
    @IBAction func addWifi(_ sender: UIButton) {
        self.mapButton.isHidden = false
        self.addWifi.isHidden = false
        self.addWifi.center = self.view.center
        self.addWifi.frame.origin.y -= 30
        self.view.addSubview(self.addWifi)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        webView.delegate = self
        let myURLString = "https://www.google.com/maps/d/viewer?mid=1Z1dI8hoBZSJNWFx2xr_MMxSxSxY"
        let request = URLRequest(url: URL(string: myURLString)!)
        webView.loadRequest(request)

        locationManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestLocation()
        }
        
        self.mapView.showsUserLocation = true
    
        let searchTableViewController = storyboard!.instantiateViewController(withIdentifier: "SearchTableViewController") as! SearchTableViewController
        self.searchController = searchTableViewController
        resultSearchController = UISearchController(searchResultsController: searchTableViewController)
        resultSearchController?.searchResultsUpdater = searchTableViewController
        resultSearchController?.delegate = self
        let searchBar = resultSearchController!.searchBar
        searchBar.placeholder = "search for airports"
        searchBar.returnKeyType = .done
        searchBar.barTintColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        navigationItem.titleView = resultSearchController?.searchBar
        navigationItem.rightBarButtonItem?.isEnabled = false
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        searchTableViewController.mapView = mapView
        searchTableViewController.handleMapSearchDelegate = self
        let historyViewController = (navigationController!.tabBarController!.viewControllers?[1] as! UINavigationController).viewControllers[0] as! HistoryViewController
        historyViewController.handleMapSearchDelegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkSubscription), name: NSNotification.Name(rawValue: "CheckSubscription"), object: nil)
        StoreManager.shared.setup()
        
        if UserDefaults.standard.bool(forKey: "isFirst") == false {
            UserDefaults.standard.set(0, forKey: "count")
            UserDefaults.standard.set(true, forKey: "isFirst")
        } else {
            self.count = UserDefaults.standard.integer(forKey: "count")
        }
        
        if UserDefaults.standard.object(forKey: "listOfUserWifi") as? [NSMutableDictionary] != nil {
            
            self.listOfUserWifi = UserDefaults.standard.object(forKey: "listOfUserWifi") as! [NSMutableDictionary]
            
            if self.listOfUserWifi.count > 0 {
                for i in 0..<self.listOfUserWifi.count {
                    let item = self.listOfUserWifi[i]
                    let annotation = CustomPointAnnotation()
                    annotation.title = item.object(forKey: "nameWifi") as? String
                    annotation.subtitle = item.object(forKey: "descriptionWifi") as? String
                    let latitude = item.object(forKey: "latitude") as! Double
                    let longitude = item.object(forKey: "longitude") as! Double
                    annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
                    annotation.number = 1
                    self.mapView.addAnnotation(annotation)
                }
            }
            
        }
        UserDefaults.standard.synchronize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkProduct()
        
//        if StoreFinder.userRatedApp() {
//            if !StoreManager.shared.receiptManager.isSubscribed {
//                UserDefaults.standard.set(false, forKey: "Subscription")
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let viewController = storyboard.instantiateViewController(withIdentifier :"AdsViewController")
//                let nav = UINavigationController(rootViewController: viewController)
//                self.present(nav, animated: false)
//                UserDefaults.standard.synchronize()
//            }
//        }
    }
    
    func checkProduct() {
        if !StoreManager.shared.isPurchased(id: nonconsumable1) {
            self.adsView.adUnitID = keyGoogleAdsBanner
            self.adsView.delegate = self
            self.adsView.rootViewController = self
            let request = GADRequest()
            self.adsView.load(request)
            self.adsView.isHidden = false
        } else {
            adsView.isHidden = true
        }
    }
    
    func checkSubscription() {
        DispatchQueue.main.async {
            print("SUBSCRIPTION ESTE")
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if self.callNumber == 0 {
            let href = webView.stringByEvaluatingJavaScript(from: "_pageData")
            let data = href?.data(using: .utf8)
            let jsonString = try? JSONSerialization.jsonObject(with: data!, options: [.allowFragments])
            self.arrayOfAirports = (((((((jsonString as! NSArray)[1] as! NSArray)[6] as! NSArray)[0] as! NSArray)[12] as! NSArray)[0] as! NSArray)[13] as! NSArray)[0] as! NSArray
            self.getInfoAirport(airports: self.arrayOfAirports)
            self.callNumber = 1
        }
    }
    
    func getInfoAirport(airports: NSArray) {
        self.callNumber = 0
        for i in 0..<airports.count {
            let dict = NSMutableDictionary()
            let airport = airports[i] as! NSArray
            let coordinates = ((airport[1] as! NSArray)[0] as! NSArray)[0] as! NSArray
            dict.setValue(coordinates, forKey: "coordinates")
            let nameAirport = (((airport[5] as! NSArray)[0] as! NSArray)[1] as! NSArray)[0]
            dict.setValue(nameAirport, forKey: "nameAirport")
            let descriptionAirport = (((airport[5] as! NSArray)[1] as! NSArray)[1] as! NSArray)[0]
            var stringDescription = descriptionAirport as! String
            if let adsLink = stringDescription.range(of: "\nOffline version of this map available!\niOS: http://apple.co/2b7BatI\nAndroid: http://bit.ly/2dDeaaN") {
                stringDescription.replaceSubrange(adsLink, with: "")
                
            }
            if let adsLink2 = stringDescription.range(of: "\nHere's how to unlock airport wireless time limits: https://foxnomad.com/2014/05/22/unlock-unlimited-wifi-airports-time-restrictions/\n") {
                stringDescription.replaceSubrange(adsLink2, with: "")
            }
            dict.setValue(stringDescription, forKey: "descriptionAirport")
            self.listOfAirports.append(dict)
        }
        
        if self.listOfAirports.count > 0 {
            for i in 0..<self.listOfAirports.count {
                let airport = self.listOfAirports[i]
                let coordinates = airport.object(forKey: "coordinates") as! NSArray
                let latitude = coordinates[0] as! Double
                let longitude = coordinates[1] as! Double
                let annotation = CustomPointAnnotation()
                annotation.title = airport.object(forKey: "nameAirport") as? String
                annotation.subtitle = airport.object(forKey: "descriptionAirport") as? String
                annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
                annotation.number = 0
                self.mapView.addAnnotation(annotation)
            }
        }
        
        (self.searchController as! SearchTableViewController).listOfAirports = self.listOfAirports
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
            
            let dictUser = NSMutableDictionary()
            dictUser.setObject(location.coordinate.latitude, forKey: "latitude" as NSCopying)
            dictUser.setObject(location.coordinate.longitude, forKey: "longitude" as NSCopying)
            UserDefaults.standard.set(dictUser, forKey: "user")
            UserDefaults.standard.synchronize()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error.localizedDescription)")
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        
        if (annotation is MKUserLocation) {
            
            return nil
        }
        
        let reuseId = "airport"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if pinView != nil {
            pinView?.annotation = annotation
            let customannotation = annotation as! CustomPointAnnotation
            pinView!.image = UIImage(named: pinImages[customannotation.number])
        } else {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            let customannotation = annotation as! CustomPointAnnotation
            pinView!.image = UIImage(named: pinImages[customannotation.number])
        }
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if !StoreFinder.userRatedApp() {
            if ((StoreManager.shared.receiptManager.isSubscribed) || self.count < 4) {
                self.count += 1
                UserDefaults.standard.set(self.count, forKey: "count")
                UserDefaults.standard.synchronize()
                
                if (view.annotation is MKUserLocation) { return }
                
                self.mapButton.isHidden = false
                self.infoView.center = self.view.center
                self.infoView.frame.origin.y -= 30
                
                let customAnnotation = view.annotation as? CustomPointAnnotation
                
                if customAnnotation?.number == 1 {
                    self.infoViewNameLabel.text = "Wifi name"
                    self.infoViewDescriptionLabel.text = "Wifi description"
                }
                
                self.nameAirport.text = (view.annotation?.title)!
                self.descriptionAirport.text = (view.annotation?.subtitle)!
                
                self.view.addSubview(self.infoView)
                mapView.deselectAnnotation(view.annotation, animated: true)
            } else {
                self.checkIfNeedShowAdsBanner()
            }
        } else {
            if StoreManager.shared.receiptManager.isSubscribed {
                if (view.annotation is MKUserLocation) { return }
                
                self.mapButton.isHidden = false
                self.infoView.center = self.view.center
                self.infoView.frame.origin.y -= 30
                
                let customAnnotation = view.annotation as? CustomPointAnnotation
                
                if customAnnotation?.number == 1 {
                    self.infoViewNameLabel.text = "Wifi name"
                    self.infoViewDescriptionLabel.text = "Wifi description"
                }
                
                self.nameAirport.text = (view.annotation?.title)!
                self.descriptionAirport.text = (view.annotation?.subtitle)!
                
                self.view.addSubview(self.infoView)
                mapView.deselectAnnotation(view.annotation, animated: true)
            } else {
                self.checkIfNeedShowAdsBanner()
            }
        }
    }
    
    @IBAction func changeModeSatelite(_ sender: UIButton) {
        switch mapType {
        case 0:
            mapView.mapType = .hybrid
            mapType += 1
        case 1:
            mapView.mapType = .satellite
            mapType += 1
        case 2:
            mapView.mapType = .standard
            mapType = 0
        default:
            break
        }
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Ads load successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Error \(error.localizedDescription)")
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
    
    func checkIfNeedShowAdsBanner() {
        if StoreFinder.userRatedApp() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"AdsViewController") as! AdsViewController
            let nav = UINavigationController(rootViewController: viewController)
            self.present(nav, animated: false)
        } else {
            if !((StoreManager.shared.receiptManager.isSubscribed) || self.count < 4) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"AdsViewController") as! AdsViewController
                let nav = UINavigationController(rootViewController: viewController)
                self.present(nav, animated: false)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.addWifi.endEditing(true)
        return true
    }
}

extension MapViewController: HandleMapSearch {
    
    func dropPinZoomIn(placemark: NSMutableDictionary){
        
        if !StoreFinder.userRatedApp() {
            if ((StoreManager.shared.receiptManager.isSubscribed) || self.count < 4) {
                self.count += 1
                UserDefaults.standard.set(self.count, forKey: "count")
                UserDefaults.standard.synchronize()
                navigationItem.rightBarButtonItem?.isEnabled = true
                navigationController?.navigationItem.rightBarButtonItem?.isEnabled = true
                
                resultSearchController?.searchBar.text = nil
                resultSearchController?.searchBar.endEditing(true)
                
                let latitude: CLLocationDegrees!
                let longitude: CLLocationDegrees!
                if placemark.object(forKey: "nameWifi") != nil {
                    latitude = placemark.object(forKey: "latitude") as! Double
                    longitude = placemark.object(forKey: "longitude") as! Double
                } else {
                    let coordinates = placemark.object(forKey: "coordinates") as! NSArray
                    latitude = coordinates[0] as! Double
                    longitude = coordinates[1] as! Double
                }
                let span = MKCoordinateSpanMake(0.1, 0.1)
                let region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(latitude, longitude), span)
                mapView.setRegion(region, animated: true)
            } else {
                self.checkIfNeedShowAdsBanner()
            }
        } else {
            if StoreManager.shared.receiptManager.isSubscribed {
                navigationItem.rightBarButtonItem?.isEnabled = true
                navigationController?.navigationItem.rightBarButtonItem?.isEnabled = true
                
                resultSearchController?.searchBar.text = nil
                resultSearchController?.searchBar.endEditing(true)
                
                let latitude: CLLocationDegrees!
                let longitude: CLLocationDegrees!
                if placemark.object(forKey: "nameWifi") != nil {
                    latitude = placemark.object(forKey: "latitude") as! Double
                    longitude = placemark.object(forKey: "longitude") as! Double
                } else {
                    let coordinates = placemark.object(forKey: "coordinates") as! NSArray
                    latitude = coordinates[0] as! Double
                    longitude = coordinates[1] as! Double
                }
                let span = MKCoordinateSpanMake(0.1, 0.1)
                let region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(latitude, longitude), span)
                mapView.setRegion(region, animated: true)
            } else {
                self.checkIfNeedShowAdsBanner()
            }
        }
    }
}
