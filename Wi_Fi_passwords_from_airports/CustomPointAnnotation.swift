//
//  CustomPointAnnotation.swift
//  Wi_Fi_passwords_from_airports
//
//  Created by User543 on 01.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class CustomPointAnnotation: MKPointAnnotation {
    
    var number: Int = 0
}
