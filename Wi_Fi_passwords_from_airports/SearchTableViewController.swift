//
//  SearchTableViewController.swift
//  Wi_Fi_passwords_from_airports
//
//  Created by User543 on 29.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import MapKit

class SearchTableViewController: UITableViewController {
    
    var matchingItems:[MKMapItem] = []
    var mapView: MKMapView? = nil
    weak var handleMapSearchDelegate: HandleMapSearch?
    
    var listOfAirports = [NSMutableDictionary]()
    
    var list = [NSMutableDictionary]()
    
    @IBOutlet var searchTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.list = []
        self.searchTable.reloadData()
    }
}

extension SearchTableViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchBarText = searchController.searchBar.text else { return }
        
        for i in 0..<self.listOfAirports.count {
            if (self.listOfAirports[i].object(forKey: "nameAirport") as! String).lowercased().contains(searchBarText.lowercased()) {
                self.list.append(self.listOfAirports[i])
                self.searchTable.reloadData()
            }
        }
    }
}

extension SearchTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell")!
        let selectedItem = self.list[indexPath.row]
        cell.textLabel?.text = selectedItem.object(forKey: "nameAirport") as? String
        return cell
    }
}

extension SearchTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.list.count > indexPath.row {
            let selectedItem = self.list[indexPath.row]
            var historyList = [NSMutableDictionary]()
            if UserDefaults.standard.object(forKey: "historyList") != nil {
                historyList = UserDefaults.standard.object(forKey: "historyList") as! [NSMutableDictionary]
            } else {
                historyList = []
            }
            historyList.insert(selectedItem, at: 0)
            UserDefaults.standard.set(historyList, forKey: "historyList")
            UserDefaults.standard.synchronize()
            handleMapSearchDelegate?.dropPinZoomIn(placemark: selectedItem)
            dismiss(animated: true, completion: nil)
        }
    }
}
