//
//  AdsViewController.swift
//  Wi_Fi_passwords_from_airports
//
//  Created by User543 on 30.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AdsViewController: UIViewController {
    
    var alert: UIAlertController?
    
    var stringSettings: String?
    
    @IBOutlet weak var premiumImageView: UIImageView!
    @IBOutlet weak var tryFreeButton: UIButton!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var closeAds: UIButton!

    
    @IBAction func infoBtn(_ sender: Any) {
        guard let webVC = storyboard?.instantiateViewController(withIdentifier: "webVC") as? WebViewController else { return }
        webVC.url = "http://devios.mobi/max_fm_res/AirPort_wif/subscription_AirPort.htm"
        self.navigationController?.pushViewController(webVC, animated: true)
        
    }
    
    @IBAction func closeAds(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        navigationController?.navigationBar.shadowImage = UIImage()
//        navigationController?.navigationBar.isTranslucent = true
        
        self.view.bringSubview(toFront: infoBtn)
        self.view.bringSubview(toFront: closeAds)
        // Do any additional setup after loading the view.
        self.addGesture()
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkSubscription), name: NSNotification.Name(rawValue: "CheckSubscription"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionTryFreeButton(_ sender: UIButton) {
        print("APASAT")
        self.goToBuy()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if StoreFinder.userRatedApp() {
            infoBtn.isHidden = false
            closeAds.isHidden = false
            let image = UIImage(named: "banner_inapp1@2x.png")
            premiumImageView.image = image
        } else {
            infoBtn.isHidden = true
            closeAds.isHidden = false
            let image = UIImage(named: "banner_inapp@2x.png")
            premiumImageView.image = image
            view.bringSubview(toFront: tryFreeButton)
        }
        
        if stringSettings == "toAdsFromSettings", stringSettings != nil {
            navigationController?.setNavigationBarHidden(false, animated: true)
            let title = UILabel()
            title.text = "PREMIUM ACCOUNT"
            title.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            title.sizeToFit()
            navigationItem.titleView = title
            navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1913671792, green: 0.4732346535, blue: 0.9469819665, alpha: 1)
            navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            navigationController?.navigationBar.isTranslucent = false
        } else {
            navigationController?.setNavigationBarHidden(true, animated: false)
        }
        animateButton()
    }
    
    func animateButton() {
//        UIView.animate(withDuration: 1.0, delay: 1.0, options: [.autoreverse, .repeat, .allowUserInteraction], animations: {
//            self.tryFreeButton.alpha = 0.3
//        }, completion: nil)
        
        UIView.animate(withDuration: 1.0, delay: 1.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
            self.tryFreeButton.alpha = 0.3
        }) { (succes) in
            if !succes {
                self.tryFreeButton.alpha = 1.0
            }
        }
    }
    
    func addGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goToBuy))
        self.premiumImageView.addGestureRecognizer(gesture)
        self.premiumImageView.isUserInteractionEnabled = true
    }
    
    func goToBuy() {
        showWaiting(completion: {
            if StoreManager.shared.productsFromStore.count > 0 {
                let product = StoreManager.shared.productsFromStore[0]
                StoreManager.shared.buy(product: product)
            } else {
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                object: nil,
                                                userInfo:["title":"Sorry", "message":"Unkown error while proccessing. Please try again!"])
            }
        })
    }
    
    func checkSubscription() {
        DispatchQueue.main.async {
            let topVC = self.getCurrentViewController(self)!
            if topVC.isKind(of: UIAlertController.self) {
                topVC.dismiss(animated: true, completion: { 
                    self.dismiss(animated: false, completion: nil)
                })
            } else {
                self.dismiss(animated: false, completion: nil)
            }
            if UserDefaults.standard.bool(forKey: "Subscription") {
                print("SUBSCRIPTION ESTE")
            }
        }
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
}
