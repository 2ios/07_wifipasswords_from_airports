//
//  SettingsViewController.swift
//  Wi_Fi_passwords_from_airports
//
//  Created by User543 on 29.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, GADBannerViewDelegate{
    
    let arrayText = ["Terms of Use", "Subscription policy", "Privacy policy", "Remove Ads Forever!","Premium Account", "Restore", "Clear history"]
    let arrayImage = [#imageLiteral(resourceName: "iRed"),#imageLiteral(resourceName: "iBlue"),#imageLiteral(resourceName: "iBlue"),#imageLiteral(resourceName: "pencil"),#imageLiteral(resourceName: "crown"), #imageLiteral(resourceName: "pro"), #imageLiteral(resourceName: "h")]
    var historyList = [NSMutableDictionary]()
    var alert: UIAlertController?
    var url: String = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var adsView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkProduct()
    }
    
    func checkProduct() {
        if !StoreManager.shared.isPurchased(id: nonconsumable1) {
            self.adsView.adUnitID = keyGoogleAdsBanner
            self.adsView.delegate = self
            self.adsView.rootViewController = self
            let request = GADRequest()
            self.adsView.load(request)
            self.adsView.isHidden = false
        } else {
            adsView.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsViewCell", for: indexPath)
        cell.textLabel?.text = self.arrayText[indexPath.row]
        cell.imageView?.image = self.arrayImage[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.url = "http://devios.mobi/max_fm_res/AirPort_wif/tou_AirPort.htm"
            performSegue(withIdentifier: "ToWebViewController", sender: nil)
        }
        
        if indexPath.row == 1 {
            self.url = "http://devios.mobi/max_fm_res/AirPort_wif/subscription_AirPort.htm"
            performSegue(withIdentifier: "ToWebViewController", sender: nil)
        }
        
        if indexPath.row == 2 {
            self.url = "http://devios.mobi/max_fm_res/AirPort_wif/privacy_AirPort.htm"
            performSegue(withIdentifier: "ToWebViewController", sender: nil)
        }
        
        if indexPath.row == 3 {
            showWaiting(completion: {
                if StoreManager.shared.productsFromStore.count > 1 {
                    let product = StoreManager.shared.productsFromStore[1]
                    StoreManager.shared.buy(product: product)
                }
            })
        }
        if indexPath.row == 4 {
            if StoreFinder.userRatedApp() {
                showWaiting(completion: {
                    if StoreManager.shared.productsFromStore.count > 0 {
                        let product = StoreManager.shared.productsFromStore[0]
                        StoreManager.shared.buy(product: product)
                    }
                })
            } else {
                performSegue(withIdentifier: "toAdsFromSettings", sender: nil)
            }
        }
        if indexPath.row == 5 {
            showWaiting(completion: {
                StoreManager.shared.restoreAllPurchases()
            })
        }
        if indexPath.row == 6 {
            showWaiting(completion: {
                if self.historyList.count > 0 {
                    self.historyList = []
                }
                UserDefaults.standard.set(self.historyList, forKey: "historyList")
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                object: nil,
                                                userInfo:["title":"Congratulations", "message":"Process done"])
            })
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToWebViewController" {
            let WebVC: WebViewController = segue.destination as! WebViewController
            WebVC.url = self.url
        }
        
        if segue.identifier == "toAdsFromSettings" {
            let ads: AdsViewController = segue.destination as! AdsViewController
            ads.stringSettings = "toAdsFromSettings"
        }
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Ads load successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Error \(error.localizedDescription)")
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
}
