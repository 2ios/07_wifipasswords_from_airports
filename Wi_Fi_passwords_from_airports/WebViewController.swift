//
//  WebViewController.swift
//  FakeGPS&SharePhoto
//
//  Created by User543 on 24.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {

    var url: String = ""
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        let title = UILabel()
        title.text = "Terms of Use"
        title.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        title.sizeToFit()
        navigationItem.titleView = title
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1913671792, green: 0.4732346535, blue: 0.9469819665, alpha: 1)
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationController?.navigationBar.isTranslucent = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let url = URL(string: self.url)
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
    }
    
}
